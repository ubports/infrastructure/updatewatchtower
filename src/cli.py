from config import *
from sources import Sources
from _gitlab import *

from uscan import *

config = Config()
sources = Sources(config)

sources.check_sources()

for p in sources.status_list():
    print(p)
