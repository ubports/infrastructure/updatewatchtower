import yaml, sys

def die(m):
    print("[FATAL] {}".format(m))

    sys.exit(1)

class Config:
    def __init__(self, config_file = "config.yml") -> None:
        self._source_base = None
        self._sources = None
        self._global = None
        self._raw_sources = None
        
        self._load(config_file)

    def _load(self, config_file) -> None:
        with open(config_file, "r") as f:
            self._config = yaml.safe_load(f)

        if not "global" in self._config:
            die("Missing global config entry")
        self._global = self._config["global"]

        if not "source_base" in self._global:
            die("Missing global.source_base config entry")
        self._source_base = self._global["source_base"]

        if not "sources" in self._config:
            die("Missing sources config entry")
        if type(self._config["sources"]) != list:
            die("Invalid source config entry, needs to be a list")
        self._raw_sources = self._config["sources"]

    def sources(self) -> list:
        if type(self._sources) == list:
            return self._sources

        self._sources = {}
        for source in self._raw_sources:
            out = source
            if type(source) == str:
                out = {
                    "package": source,
                    "type": "native",
                    'autoupdate': False,
                }

            if not "autoupdate" in out:
                out["autoupdate"] = False

            if not "type" in out:
                out["type"] = "native"

            if out["package"].startswith("git@") or out["package"].startswith("http"):
                out["name"] = out["package"].split("/")[-1].replace(".git", "")
                self._sources[out["package"]] = out
            else:
                out["name"] = out["package"]
                out["package"] = self._source_base.format(out["package"])
                self._sources[out["package"]] = out

        return self._sources
