import subprocess
import xmltodict, os

def uscan_github_tag(package, path, file):
    template = "version=4 \n \
                opts=filenamemangle=s/.+\/v?(\d\S+)\.tar\.gz/{package}-$1\.tar\.gz/ \n \
                https://github.com/{path}/tags .*/v?(\d\S+)\.tar\.gz"
    
    output = template.format(package=package, path=path)

    with open(file, "w") as f:
        f.write(output)

    return file


def uscan_create_watch(package_conf, file):
     if (package_conf["type"] == "github+tag"):
        return uscan_github_tag(package_conf["package"], package_conf["path"], file)
     
     return False

def uscan_try_to_make_from_source_loc(path):
    fil = "{}/debian/ubports.source_location".format(path)

    if not os.path.exists(fil):
        return False

    with open(fil, "r") as f:
        url = f.readline()

    pat = False

    if url.startswith("http") and "://api.github.com/repos/" in url:
        sp = url.split("://api.github.com/repos/")[1].split("/")
        pat = {
            "path": "{}/{}".format(sp[0], sp[1]),
            "type": "github+tag"
        }

    if url.startswith("http") and "://github.com/" in url:
        sp = url.split("://github.com/")[1].split("/")
        pat = {
            "path": "{}/{}".format(sp[0], sp[1]),
            "type": "github+tag"
        }

    return pat


def uscan_check(path, fallback_package):
    result = subprocess.run(["uscan", "--dehs", "--no-download"], cwd=path, capture_output=True, text=True)

    raw_output = xmltodict.parse(result.stdout)

    if "dehs" not in raw_output:
        return {
            "status": "No valid uscan output",
            "error": True,
            "upstream-url": "",
            "upstream-version": "uscan error",
            "debian-mangled-uversion": "uscan error",
            "debian-uversion": "uscan error",
            "package": fallback_package,
            "_package": fallback_package,
            "warnings": "No valid uscan output"
        }

    output = raw_output["dehs"]

    if (uscan_has_error(output)):
        return {
            "status": output["warnings"],
            "error": True,
            "upstream-url": "",
            "upstream-version": "uscan error",
            "debian-mangled-uversion": "uscan error",
            "debian-uversion": "uscan error",
            "package": fallback_package,
            **output
        }

    if "status" not in output:
        return {
            "status": "No valid uscan output",
            "error": True,
            "upstream-url": "",
            "upstream-version": "uscan error",
            "debian-mangled-uversion": "uscan error",
            "debian-uversion": "uscan error",
            "package": fallback_package,
            "_package": fallback_package,
            **output
        }
    
    output = {
        "error": False,
        "package": fallback_package,
        **output
    }

    return output

def uscan_is_up_to_date(result):
    return result["status"] == "up to date"

def uscan_has_error(result):
    return "warnings" in result