import sys
from flask import Flask, render_template
from flask_frozen import Freezer

app = Flask(__name__)
freezer = Freezer(app)

from config import *
from sources import Sources
from deb_archive import *
import datetime
date = datetime.date.today()

config = Config()
sources = Sources(config)

sources.check_sources()

s = get_sources("http://repo.ubports.com", "devel-noble", "main", T=BINARY)
s2 = get_sources("http://archive.ubuntu.com/ubuntu/", "noble", "main")
s3 = get_sources("http://archive.ubuntu.com/ubuntu/", "noble", "universe")

debs = compeare_sources(s, s2, s3)

@app.route("/")
def index():
    return render_template("page.html", sources=sources.status_list(), debs=debs, date=date)

if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "build":
        freezer.freeze()
    else:
        app.run(port=8000)
