from debian.deb822 import Sources as DebSources
import requests
import os, apt_pkg, sys


SOURCES = "{}/dists/{}/{}/source/Sources.gz"
BINARY = "{}/dists/{}/{}/binary-amd64/Packages.gz"

def download(url, output):
    get_response = requests.get(url,stream=True)
    with open(output, 'wb') as f:
        for chunk in get_response.iter_content(chunk_size=1024):
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)

    return output

def get_sources(archive, suite, component, T=SOURCES):
    print(T.format(archive, suite, component))

    if not os.path.exists(".apt"):
        os.mkdir(".apt")

    p = archive.replace("http://", "").replace("https://", "").replace("/", "")
    filename = "{}-{}-{}".format(p, suite, component)
    path = os.path.realpath("{}/../.apt/{}".format(os.path.dirname(__file__), filename))

    return download(T.format(archive, suite, component), path)

def to_hasmap(s1):
    ss1 = {}
    with open(s1) as f:
        for src in DebSources.iter_paragraphs(f, use_apt_pkg=True):
            if not "Source" in src:
                p = src["Package"]
            else:
                p = src["Source"]
            if p in ss1:
                if apt_pkg.version_compare(src["Version"], ss1[p]) > 0:
                    ss1[p] = src["Version"]
            else:
                ss1[p] = src["Version"] 

    return ss1

def compeare_sources(s1, s2, s3):
    ss2 = {
        **to_hasmap(s2),
        **to_hasmap(s3)
    }

    ss1 = to_hasmap(s1)

    output = []
    for package, version in ss1.items():
        if package in ss2:
            ver = apt_pkg.version_compare(version, ss2[package])
            output.append({
                "package": package,
                "upstream_version": ss2[package],
                "package_version": version,
                "version_compeare": ver,
                "uptodate": ver >= 0,
                "status": "Up to date" if ver >= 0 else "newer package available"
            })

    return output