import gitlab, os

class Gitlab:
    def __init__(self) -> None:
        token = os.getenv("UWT_TOKEN")

        self._gitlab = gitlab.Gitlab(private_token=token)

    def mr(self, project, source, target, title, content, labels):
        project = self._gitlab.projects.get('ubports/development/core/packaging/{}'.format(project))

        mr = project.mergerequests.create({'source_branch': source,
                                   'target_branch': target,
                                   'title': title,
                                    'description': content,
                                   'labels': labels})
        
        # return url to mr
        return mr.web_url

    # get existing mr with tag
    def get_mr(self, project, branch):
        project = self._gitlab.projects.get('ubports/development/core/packaging/{}'.format(project))

        mrs = project.mergerequests.list(state="opened")

        for mr in mrs:
            if mr.source_branch == branch:
                return mr.web_url

        return None

    def get_subproject_projects(self, sub):
        pack = self._gitlab.groups.get(sub)
        return pack.projects.list(get_all=True)