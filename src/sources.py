from uscan import *

from git import Repo
from _gitlab import Gitlab

import os, shutil, sys, re, tempfile

import debian.changelog

class Source:
    def __init__(self, config) -> None:
        self._uscan = None
        self._url = config["package"]
        self._config = config
        self._gl = Gitlab()

        self._package = self._url.split("/")[-1].replace(".git", "")

        self._path = os.path.realpath("{}/../.sources/{}".format(os.path.dirname(__file__), self._package))

        if not os.path.exists(".sources"):
            os.mkdir(".sources")

        if os.path.exists(self._path):
            self._repo = Repo(self._path)
            self._repo.remotes.origin.pull()
        else:
            self._repo = Repo.clone_from(self._url, to_path=self._path)

    def update_and_push(self):
        url = self._uscan["upstream-url"]
        filename = os.path.basename(url)

        # TODO: make this configurable
        #       allow different version style (git etc)
        ver = "{}-0ubports1".format(self._uscan["upstream-version"])
        clean_ver = re.sub("[^a-zA-Z0-9]", "_", self._uscan["upstream-version"])

        ch_file = os.path.join(self._path, "debian/changelog")

        if not os.path.exists(ch_file):
            print("WARNING! Did not find changelog entry")
            return None


        branch = "personal/ubports_uwt_bot/update-{}".format(clean_ver)
        # check if mr exists already
        mr = self._gl.get_mr(self._config["name"], branch)
        if mr:
            print("MR already exists")
            return mr

        # Start new branch
        old_branch = self._repo.active_branch

        self._repo.create_head(branch)
        self._repo.heads[branch].checkout()

        new_branch = self._repo.active_branch

        with open(ch_file, "r") as f:
            ch = debian.changelog.Changelog(file=f)
        print(ch.package)
        ch.new_block(
            version=ver,
            # TODO: make this configurable
            author="UBports package upgrader bot <dev@ubports.com>",
            package=ch.package,
            distributions="UNRELEASED",
            date=debian.changelog.format_date()

        )
        # TODO: make this configurable
        ch.add_change("")
        ch.add_change("  * New upstream release v{} ".format(self._uscan["upstream-version"]))
        ch.add_change("")

        with open(ch_file, "w") as f:
            ch.write_to_open_file(f)

        # edit debian/ubports.source_location to add the new version
        loc_file = os.path.join(self._path, "debian/ubports.source_location")

        with open(loc_file, "w") as f:
            f.writelines([self._uscan["upstream-url"], "\n", "{}_{}.orig.tar.gz".format(ch.package, self._uscan["upstream-version"]), "\n"])

        # git commit
        self._repo.index.add("debian/changelog")
        self._repo.index.add("debian/ubports.source_location")

        if self.check_compat():
            self._repo.index.add("debian/compat")

        os.environ['GIT_COMMITTER_NAME'] = "UBports Updater Bot"
        os.environ['GIT_COMMITTER_EMAIL'] = "dev@ubports.com"
        os.environ['GIT_AUTHOR_NAME'] = "UBports Updater Bot"
        os.environ['GIT_AUTHOR_EMAIL'] = "dev@ubports.com"

        self._repo.index.commit("[UBports] New upstream release v{} ".format(self._uscan["upstream-version"]))

        username = "UBportsJenkinsService"
        password = os.getenv("UWT_TOKEN")

        # add username and password to remote url
        url = self._repo.remotes.origin.url
        split_url = url.split("://")
        remote = "{}://{}:{}@{}".format(
            split_url[0],
            username,
            password,
            split_url[1]
        )
        self._repo.remotes.origin.set_url(remote)

        self._repo.remotes.origin.push(branch)

        # change back to old branch
        old_branch.checkout()

        # Format a nice title and content for the MR including the changelog and version from and to
        title = "[UBports] New upstream release v{} ".format(self._uscan["upstream-version"])
        content = "New upstream release v{} ".format(self._uscan["upstream-version"])
        content += "\n\n"
        content += "This updates:\n"
        content += "from: {}\n".format(self._uscan["debian-uversion"])
        content += "to: {}\n".format(self._uscan["upstream-version"])
        content += "\n"
        content += "New debian version: {}\n".format(ver)

        mr_url = self._gl.mr(self._config["name"], new_branch.name, old_branch.name, title, content, ["updatewatchtower"])

        return mr_url

    def check_compat(self):
        compat_file = os.path.join(self._path, "debian/compat")

        if os.path.exists(compat_file):
            with open(compat_file, "r") as f:
                line = f.readline()
                version = int(line.strip())
                if version < 9:
                    with open(compat_file, "w") as f:
                        f.write("9\n")
                    return True
        # else we dont need
        return False

    def check(self):
        print("Checking {}".format(self._package))
        watch_type = "From git repo"

        watchfile = "{}/debian/watch".format(self._path)
        if self._config["type"] != "native":
            uscan_create_watch(self._config, watchfile)
            watch_type = "Created from config file"

        if not os.path.exists(watchfile):
            print("no watch file, trying source loc")
            loc = uscan_try_to_make_from_source_loc(self._path)
            print(loc)
            if loc:
                print("no watch file, creating ")
                self._config["type"] = loc["type"]
                self._config["path"] = loc["path"]
                uscan_create_watch(self._config, watchfile)
                watch_type = "Created from ubports source location file"


        self._uscan = uscan_check(self._path, self._package)
        self._uscan["uptodate"] = self.is_up_to_date()
        self._uscan["_local_path"] = self._path
        self._uscan["_watch_type"] = watch_type

        if self._config["autoupdate"] and not self._uscan["uptodate"]:
            print("Updating and pushing")
            mr_url = self.update_and_push()
            self._uscan["mr"] = mr_url
        else:
            # if uptodate print that info
            if self._uscan["uptodate"]:
                print("Package is up to date")
            # not not auto update, print that
            if not self._config["autoupdate"]:
                print("Package is not set to auto update")
            self._uscan["mr"] = None

    def is_up_to_date(self) -> bool:
        return uscan_is_up_to_date(self._uscan)

    def status(self) -> dict:
        return self._uscan

class Sources:
    def __init__(self, config) -> None:
        self._sources = {}
        self._config = config

    def get_source(self, source) -> Source:
        if source["package"] in self._sources:
            return self._sources[source["package"]]

        self._sources[source["package"]] = Source(source)
        return self._sources[source["package"]]

    def get_source_by_name(self, name) -> Source:
        if name in self._sources:
            return self._sources[name]

        for _, source in self._config.sources().items():
            if source["name"] == name:
                return self.get_source(source)
        return None

    def check_sources(self):
        for _, source in self._config.sources().items():
            source_c = self.get_source(source)
            source_c.check()

    def status(self):
        status = {}
        for _, source in self._config.sources().items():
            source_c = self.get_source(source)
            status[source["package"]] = source_c.status()
        return status

    def status_list(self):
        status = []
        for _, source in self._config.sources().items():
            source_c = self.get_source(source)
            ss = source_c.status()
            ss["git"] = source["package"]
            status.append(ss)
        return status
